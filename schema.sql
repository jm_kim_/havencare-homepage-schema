
select "board";

CREATE TABLE board (
  board_code VARCHAR(21) NOT NULL,
  board_type VARCHAR(32) NOT NULL,
  board_title VARCHAR(512) NOT NULL,
  board_text TEXT NOT NULL,
  view_start_date CHAR NOT NULL,
  view_end_date CHAR NOT NULL,
  view_count INT NOT NULL DEFAULT 0,
  insert_dt VARCHAR(80) NOT NULL,
  update_dt VARCHAR(80) NOT NULL,
  delete_dt VARCHAR(80) NOT NULL
);

ALTER TABLE board ADD PRIMARY KEY(board_code);
